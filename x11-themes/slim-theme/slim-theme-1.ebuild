# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=2
inherit eutils

DESCRIPTION="SLiM (Simple Login Manager) theme"
#HOMEPAGE=""
SRC_URI="ftp://31.41.246.91/my/cl-slim/cl-slim.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 arm ~mips ppc ppc64 sparc x86 ~x86-fbsd"
IUSE=""

RDEPEND="x11-misc/slim"
DEPEND=""

RESTRICT="strip binchecks"

S="${WORKDIR}"

src_install() {
	local themesdir="/usr/share/slim/themes/cl-slim"
	insinto ${themesdir}
	doins -r .
}
