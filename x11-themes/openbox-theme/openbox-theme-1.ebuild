# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=2
inherit eutils

DESCRIPTION="OpenBox theme"
#HOMEPAGE=""
SRC_URI="ftp://31.41.246.91/my/cl-openbox/cl-openbox.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="x11-wm/openbox"
DEPEND=""

RESTRICT="strip binchecks"

S="${WORKDIR}"

src_install() {
	local themesdir="/usr/share/themes"
	insinto ${themesdir}
	doins -r .
}
