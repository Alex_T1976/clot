#Calculate path=/root
#?in(os_linux_pkglist, clot)!=#
cl-update -s
cl-update -s && emerge -uDN world
grub-mkconfig -o /boot/grub/grub.cfg
watch -n3 'df -h /var/calculate/tmp/portage;df -hi /var/calculate/tmp/portage'
mount -t tmpfs none /var/calculate/tmp/portage -o nr_inodes=2M,size=5G,mode=01770,uid=portage,gid=portage
tmux attach || tmux new
cd /root/mrim && ./mrim.sh -c mrim.conf
#in#

