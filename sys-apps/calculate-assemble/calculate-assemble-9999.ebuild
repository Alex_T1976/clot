# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
PYTHON_COMPAT=(python2_7)

inherit git-2 distutils-r1 eutils

SRC_URI=""
EGIT_REPO_URI="git://31.41.246.91/calculate-assemble.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86"

DEPEND="~sys-apps/calculate-builder-2.2.32"

RDEPEND="${DEPEND}"

src_prepare() {
        cd "${S}"

	# fix rebuild changed packages
	epatch "${FILESDIR}/calculate-assemble-2.2.32-fix_rebuild.patch"

	# fix assemble path
	epatch "${FILESDIR}/calculate-assemble-2.2.32-fix_assemble.patch"

	# remove nvidia 173
	epatch "${FILESDIR}/calculate-assemble-2.2.32-remove_nvidia_173.patch"
}
