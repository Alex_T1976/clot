# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="Syslinux background for Calculate Linux"
#HOMEPAGE="http://www.calculate-linux.org/packages/media-gfx/syslinux-themes-calculate"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

SRC_URI="ftp://31.41.246.91/my/syslinux-cl/syslinux-calculate-15.tar.bz2"

RDEPEND="!<sys-boot/calcboot-4.05.0-r1"

DEPEND="${RDEPEND}"

src_install() {
	insinto /
	doins -r .
}

